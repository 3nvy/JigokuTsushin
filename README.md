# Jigoku Tsushin Midnight Fan Page

This is a fan made Hellish Midnight Page from Jigoku Shoujo Anime in Laravel 5.3. Intended as normal behavior, only at midnight will the real page show, besides that, only an error message. To check the functionality you can type '/always' as a route to force the real page to load :)

Have fun tricking your friends ^^

@All Rights of the Anime reserved to its creator.
