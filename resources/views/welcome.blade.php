<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>Jigoku Tsushin</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">

    <script src="https://code.jquery.com/jquery-3.1.0.min.js"
            integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.4/jstz.min.js'></script>

    <script src="{{ elixir('js/app.js') }}"></script>

</head>
<body>

<div class="container fill">

    <div class="form flex fill" style="display:none; justify-content: initial; flex-direction: column">

        <img src="/assets/homeimage.png" style="margin:75px 0 10px 0;">

        <p style="font-size: 18px;">Your grievance shall be avenged.</p>

        <form class="hellForm flex" action="/"
              style="flex-direction: column; color: black; align-items: center; margin-top:25px;">
            <input class="hatred" type="text" style="width:300px; margin-bottom: 10px;">
            <button class="sendToHell">Send</button>
        </form>

    </div>


    <div class="contract flex fill" style="display:none;">
        <img src="/assets/puppet.jpg">
    </div>

    <div class="flame flex fill">
        <img src="https://cdn.discordapp.com/attachments/107819425649930240/223135858084151296/indice.gif">
    </div>

</div>

<script>

    $(function(){var a=new Audio("/assets/flame.mp3");a.addEventListener("ended",function(){$(".flame").hide(),$(".form").fadeIn(),a=new Audio("/assets/hellgirl.mp3"),setTimeout(function(){a.play()},2e3)}),a.play(),$(".sendToHell").click(function(b){b.preventDefault(),""!==$(".hatred").val()&&(a.pause(),a=new Audio("/assets/link_accept.mp3"),a.addEventListener("ended",function(){$(".hellForm").submit()}),$(".form").hide(),$(".contract").fadeIn(),a.play())})});


</script>

</body>
</html>
