<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class PageController extends Controller
{
    public function index()
    {

//        $locale = explode('-', explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE'])[0])[1];


        if (Carbon::now('Europe/Lisbon')->format('H:i') === '00:00') {
            return view('welcome');
        }

        return view('error');

    }

    public function always(){
        return view('welcome');
    }
}
